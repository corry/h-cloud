package com.hcloud.auth.clientdetails.service.impl;

import com.hcloud.auth.clientdetails.entity.ClientEntity;
import com.hcloud.auth.clientdetails.repository.ClientRepository;
import com.hcloud.auth.clientdetails.service.ClientService;
import com.hcloud.auth.service.impl.HcloudJdbcClientDetailsService;
import com.hcloud.common.core.constants.CoreContants;
import com.hcloud.common.core.exception.ServiceException;
import com.hcloud.common.crud.service.impl.BaseDataServiceImpl;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * @Auther hepangui
 * @Date 2018/10/31
 */
@Service
public class ClientServiceImpl extends  BaseDataServiceImpl<ClientEntity,ClientRepository>
        implements ClientService {

    private BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();


    @Override
    public void modifyPass(String id, String oldPsw, String newPsw) {

        ClientEntity clientEntity = this.get(id);
        if (clientEntity == null) {
            throw new ServiceException("找不到客户端");
        }
        if (oldPsw != null && passwordEncoder.matches(oldPsw, clientEntity.getClientSecret())) {
            clientEntity.setClientSecret(passwordEncoder.encode(newPsw));
            this.update(clientEntity);
            HcloudJdbcClientDetailsService.clearCache(clientEntity.getClientId());
        } else {
            throw new ServiceException("旧密码错误");
        }
    }

    @Override
    public void resetPass(String userId) {
        if (userId == null) {
            throw new ServiceException("找不到Client");
        }
        ClientEntity clientEntity = this.get(userId);
        if (clientEntity == null) {
            throw new ServiceException("找不到Client");
        }
        clientEntity.setClientSecret(passwordEncoder.encode(CoreContants.DEFAULT_PASSWORD));
        this.update(clientEntity);
        HcloudJdbcClientDetailsService.clearCache(clientEntity.getClientId());
    }

    @Override
    public ClientEntity add(ClientEntity entity) {
        if (entity != null && entity.getClientId() != null) {
            entity.setClientSecret(new BCryptPasswordEncoder().encode(CoreContants.DEFAULT_PASSWORD));
        }
        return super.add(entity);
    }

    @Override
    public ClientEntity update(ClientEntity entityNew) {
        ClientEntity entity = this.get(entityNew.getId());
        if (entity == null) {
            throw new ServiceException("缺少用户id");
        }
        String password = entity.getClientSecret();
        BeanUtils.copyProperties(entityNew, entity);
        entity.setClientSecret(password);
        if ("".equals(entityNew.getAdditionalInformation())) {
            entity.setAdditionalInformation(null);
        }
        super.update(entity);
        HcloudJdbcClientDetailsService.clearCache(entityNew.getClientId());
        return entityNew;
    }

}
