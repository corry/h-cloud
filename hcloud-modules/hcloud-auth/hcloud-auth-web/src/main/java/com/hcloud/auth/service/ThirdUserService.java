package com.hcloud.auth.service;

import com.hcloud.auth.api.user.HcloudUserDetails;
import com.hcloud.auth.api.config.LoginType;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
public interface ThirdUserService {
    HcloudUserDetails loadUser(String para, LoginType loginType);
}
