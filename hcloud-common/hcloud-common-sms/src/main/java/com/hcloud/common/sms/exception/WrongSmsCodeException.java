package com.hcloud.common.sms.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
public class WrongSmsCodeException extends AuthenticationException {
    public WrongSmsCodeException(String msg) {
        super(msg);
    }
    public WrongSmsCodeException(){
        this("短信验证码有误");
    }
}
