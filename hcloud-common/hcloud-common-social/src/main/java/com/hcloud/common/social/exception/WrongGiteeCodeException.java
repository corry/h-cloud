package com.hcloud.common.social.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
public class WrongGiteeCodeException extends AuthenticationException {
    public WrongGiteeCodeException(String msg) {
        super(msg);
    }

    public WrongGiteeCodeException() {
        this("gitee登录信息错误");
    }
}
