package com.hcloud.common.social.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
@Data
@Component
@ConfigurationProperties(prefix = "hcloud.social.weixin")
public class WeixinConfig {
    private String appId;
    private String appSecret;
}
