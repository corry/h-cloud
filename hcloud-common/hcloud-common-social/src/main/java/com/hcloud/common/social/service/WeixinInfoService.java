package com.hcloud.common.social.service;

/**
 * @Auther hepangui
 * @Date 2018/12/13
 */
public interface WeixinInfoService {
    String getOpenId(String code);
}
